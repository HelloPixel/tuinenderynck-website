    <footer id="footer" class="site-footer" role="contentinfo">

        <?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?>

        <div class="site-info">
            <p>
                Copyright <?php Date("Y"); ?> <a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
            </p>
        </div> <!-- .site-info -->
    </footer> <!-- .site-footer -->

    </div> <!-- .site -->

    <?php wp_footer(); ?>

    </body>

    </html>