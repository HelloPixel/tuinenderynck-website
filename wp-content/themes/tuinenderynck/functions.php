<?php
if (function_exists('register_sidebar'))
    register_sidebar();

/** * Hides the custom post template for pages on WordPress 4.6 and older * * @param array $post_templates Array of page templates. Keys are filenames, values are translated names. * @return array Filtered array of page templates. */ function makewp_exclude_page_templates($post_templates)
{
    if (version_compare($GLOBALS['wp_version'], '4.7', '<')) {
        unset($post_templates['templates/my-full-width-post-template.php']);
    }
    return $post_templates;
}
add_filter('theme_page_templates', 'makewp_exclude_page_templates');

// Loading Custom CSS and JS
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('main', get_stylesheet_directory_uri() . '/assets/dist/main.bundle.js', [], '1.0.0', true);
    wp_enqueue_style('main', get_stylesheet_directory_uri() . '/assets/dist/main.css', [], '1.0.0', 'all');
});

// Enabling support for Featured Image
add_theme_support('post-thumbnails');

// Navigation menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'header-menu' => __('Header Menu'),
            'footer-menu' => __('Footer Menu')
        )
    );
}
add_action('init', 'register_my_menus');
