<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="page" class="hfeed site">
        <a class="skip-link screen-reader-text" href="#content"><?php _e('Skip to content', 'tuinenderynck'); ?></a>
        <header id="masthead" class="site-header" role="banner">
            <div class="site-branding">
                <h1 class="site-title">
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                        <img src="<?php echo get_theme_file_uri("assets/dist/images/logo.svg"); ?>" alt="<?php bloginfo('name'); ?>" />
                    </a>
                </h1>
                <?php
                $description = get_bloginfo('description', 'display');
                if ($description || is_customize_preview()) :
                    echo $description;
                endif; ?>
                <?php wp_nav_menu(array(
                    'theme_location' => 'header-menu',
                    'container' => 'nav',
                    'container_class' => 'nav nav-primary',
                    'menu_class' => 'menu menu-primary'
                )); ?>
            </div><!-- .site-branding -->
        </header><!-- .site-header -->
        <div id="content" class="site-content">