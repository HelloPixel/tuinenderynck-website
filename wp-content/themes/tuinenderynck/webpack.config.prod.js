const { merge } = require("webpack-merge");
const common = require("./webpack.config.common");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");

module.exports = merge(common, {
    mode: "production",
    module: {
        rules: [
            {
                test: /\.s?css$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    }, 
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 1,
                            esModule: false,
                        },
                    }, 
                    "postcss-loader"
                ],
            }
        ]
    },
    optimization: {
        minimizer: [
            `...`,
            new CssMinimizerPlugin(),
        ]
    },
    plugins: [new MiniCssExtractPlugin()]
});